import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Layout from './components';

function App() {
  return (
      <Layout />
  );
}

export default App;
