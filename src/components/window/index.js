import React, { useState, useRef, useEffect } from 'react';

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import CloseIcon from '@material-ui/icons/Close';
import MaximizeIcon from '@material-ui/icons/Maximize';
import MinimizeIcon from '@material-ui/icons/Minimize';
import RemoveIcon from '@material-ui/icons/Remove';
import DragHandleIcon from '@material-ui/icons/DragHandle';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';
import SettingsIcon from '@material-ui/icons/Settings';

import SimpleMenu from '../simple-menu';


const useStyles = makeStyles((theme) => ({
    window: {
        flexGrow: 1,
        padding: 0,
        display: 'flex',
        background: '#222',
        color: '#ccc',
        height: '100%',
        flexFlow: 'column',
        borderRadius: 0,
        cursor: 'context-menu'
    },
    titleBar: {
        padding: theme.spacing(0),
        textAlign: 'center',
        //color: theme.palette.text.secondary,
        background: '#484848',
        color: '#ccc',
        borderRadius:0,
        height: 32,
        maxHeight: 32,
        justifyContent: 'space-between',
        flexFlow: 'row',
        display: 'flex',
        userSelect: 'none'
    },
    body: {
        padding: theme.spacing(0),
        textAlign: 'center',
        //color: theme.palette.text.secondary,
        background: '#303030',
        color: '#ccc',
        alignSelf: 'stretch',
        height:'100%',
        borderRadius:0
    },
    footer: {
        padding: theme.spacing(0),
        textAlign: 'center',
        //color: theme.palette.text.secondary,
        background: '#242424',
        color: '#ccc',
        borderRadius:0,
        maxHeight: 40
      },
      button: {
        color: '#ccc',
        borderRadius: 0
      },
      titleBarButton: {
        color: '#999',
        borderRadius: 0,
        minWidth: '44px',
        fontSize: '13px'
      },
      dragHandleButton: {
        color: '#656565',
        borderRadius: 0,
        paddingLeft: 0,
        paddingRight: 0,
        minWidth: '20px'
        
      },
      draggableHandleIcon: {
        transform: 'rotate(90deg)'
      },
      title:{
        padding: '6px',
        fontSize: '13px'
      },
      fullscreen:{
          position: 'fixed',
          zIndex: 100000,
          left: 0,
          right: 0,
          top: 0,
          bottom: 0
      }
  }));

  const initialState = {
    mouseX: null,
    mouseY: null,
  };

export default function Window({children, options={title:'Window'}, layoutInfo, rest}) {

    const classes = useStyles();
    const simpleMenuRef = useRef();
   // const [settings, setSettings] = useState();
    const [state, setState] = React.useState(initialState);
    const [fullscreen, setFullscreen] = React.useState(false);
    
    const onTitleIconClick = (event) => {
        simpleMenuRef.current.open(event);
    }

    const toggleFullscreen = () => {
        setFullscreen(!fullscreen);
    }

    useEffect(() => {

    }, [state])

    return (
        <div className={`${classes.window} ${fullscreen ? classes.fullscreen : ''}`} >
            <Paper className={classes.titleBar}>
                <div>
                    <Button className={classes.dragHandleButton} ><DragHandleIcon fontSize="small" className={classes.draggableHandleIcon} /></Button>
                </div>
                <div className={classes.title}>{options.title}</div>
                <div>
                    <Button onClick={onTitleIconClick} className={classes.titleBarButton} aria-controls="simple-menu" aria-haspopup="true" ><SettingsIcon fontSize="small" /></Button>
                    <Button className={classes.titleBarButton} ><RemoveIcon fontSize="small" /></Button>
                    <Button onClick={toggleFullscreen} className={classes.titleBarButton} >{ fullscreen ?  <FullscreenExitIcon fontSize="small" /> : <FullscreenIcon fontSize="small" />}</Button>
                    <Button className={classes.titleBarButton} ><CloseIcon fontSize="small" /></Button>
                    {options.windowMenu && options.windowMenu.optionsMenu &&
                        <SimpleMenu ref={simpleMenuRef} items={options.windowMenu.optionsMenu} layoutInfo={layoutInfo} />
                    }
                </div>
            </Paper>
            <Paper className={classes.body}>
                {children} 
            </Paper>
            <Paper className={classes.footer}>
                <Button className={classes.button} >1</Button>
                <Button className={classes.button} >2</Button>
                <Button className={classes.button} >3</Button>
            </Paper>
        </div>
    );
}