import React, { useState, useEffect, Component } from 'react';

// import Box from '@material-ui/core/Box';
// import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Window from '../components/window';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { v4 as uuid } from 'uuid';
import clone from 'rfdc';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      padding: '0',
      display: 'flex',
      alignItems: 'stretch',
      background: '#222'
    }
  }));
  
  // lint-ignore-next-line
  function createLayoutModel() {
    return { 
        layoutId: uuid(),
        rows: [
            {
                columns: [
                    createColModel({title:'New Window'})
                ]
            }
        ]
    }
  }

  function createRowModel() {
    return { 
        rowId: uuid(),
        height: null,
        maxWidth: 'none',
        columns: [createColModel()]
    }
  }

  function createColModel() {
    return { 
        colId: uuid(),
        width: '100%',
        component:  'Window'
    }
  }

  const initialState = {
    mouseX: null,
    mouseY: null,
    colIndex: null,
    rowIndex: null,
    row: null,
    col: null
  };


export default function Layout() {

    const classes = useStyles();
    const [state, setState] = React.useState(initialState);
    const [windowMenu, setWindowMenu] = React.useState({
            optionsMenu: [
                {
                    label: 'Add Row',
                    action: addRow
                },
                {
                    label: 'Add Column',
                    action: addColumn
                },
                {
                    label: 'Delete Row',
                    action: deleteRow
                },
                {
                    label: 'Delete Column',
                    action: deleteColumn
                }
            ]
    });
    const [layout, setLayout] = useState(
        {
            layoutId: uuid(),
            rows: [
                {   
                    rowId: uuid(),
                    height: '400px',
                    columns: [
                       createColModel()
                    ]
                },
                {
                    rowId: uuid(),
                    columns: [
                        createColModel(),
                        createColModel()
                    ]
                },
                {
                    rowId: uuid(),
                    columns: [
                        createColModel(),
                        createColModel(),
                        createColModel()
                    ]
                },
                {
                    rowId: uuid(),
                    columns: [
                        createColModel(),
                        createColModel(),
                        createColModel(),
                        createColModel()
                    ]
                }
            ]
        }
    );

    // const createLayout = (options) => {
        
    //     options = {
    //         rows:  [
    //             {
    //                 columns: [

    //                 ]
    //             }
    //         ]
    //     }

    // }
    
    function addRow(options={rowIndex:null}) {

        let row = createRowModel();
        let index =  options.rowIndex || state.rowIndex;
        layout.rows.splice(index + 1, 0, row);
        
        setLayout({...layout, rows: layout.rows});
        closeContextMenu();
        // todo: notify server
        // server.layout.addRow(layout.layoutId, row);
    }
    
    function deleteRow({rowIndex, colIndex, row, col}) {
        let rowId = row ? row.rowId : state.row ? state.row.rowId : null;
        let rows = JSON.parse(JSON.stringify(layout.rows));
        layout.rows = rows.filter(r => r.rowId !== rowId);
        setLayout({...layout});
        closeContextMenu();
        // todo: notify server
        // server.layout.deleteRow(layout.layoutId, state.row.rowId);
    };
    
    function addColumn(options={rowIndex:null, colIndex:null, row:null, col:null}) {
        let {rows} = layout;
        let rIndex =  options.rowIndex || state.rowIndex;
        let cIndex =  options.colIndex || state.colIndex;
        const newCol = createColModel();

        let count = rows[rIndex].columns.length;
        if (count < 12) {
            rows[rIndex].columns.splice(cIndex + 1, 0, newCol);
            layout.rows = rows;
            setLayout({...layout});
            closeContextMenu();
        }
        else {
            // todo: notify user
            // alert('Layout supports a maximun of 12 Columns');
        }
    };

    function deleteColumn({rowIndex, colIndex, row, col}) {
        let l = {...layout};
        let removedCol = l.rows[rowIndex].columns.splice(colIndex, 1);
        setLayout(l);
        closeContextMenu();
    }

    function getComponent({componentName, options, layoutInfo, rest}){
        let component;
        switch(componentName){
            case 'table': break;
            default: component = <div className="flex-center">{layoutInfo.col.colId}</div>;
        }
        return (
            <Window options={{...options, windowMenu}} layoutInfo={layoutInfo} {...rest} >
                {component}
            </Window>
        )
    }
    
    useEffect(() => {

        // addRow(1);
       
    }, [windowMenu, layout])

    const onGridClick = (event, rowIndex, colIndex, row, col) => {
        openContextMenu(event, rowIndex, colIndex, row, col);
    }

    const openContextMenu = (event, rowIndex, colIndex, row, col) => {
        event.preventDefault();
        setState({
          mouseX: event.clientX - 2,
          mouseY: event.clientY - 4,
          rowIndex, 
          colIndex,
          row, 
          col
        });
    }

    const closeContextMenu = () => {
        setState(initialState);
    };
   
    let count = 0;

    return(
        <div className={classes.root} onContextMenu={(e)=>{ e.preventDefault() }}  >

            <Grid container spacing={0}>

                {layout && layout.rows && layout.rows.map((row, rowIndex) => { return (
                    
                        <Grid container key={`row-${rowIndex}`} >

                            {row.columns.map((col, colIndex, arr) => { count++; return (

                                <Grid item
                                    onContextMenu={(event) => onGridClick(event, rowIndex, colIndex, row, col)} 
                                    key={`col-${rowIndex}-${colIndex}`} 
                                    xs={12} 
                                    sm={12} 
                                    md={ Math.round(12/arr.length)} 
                                    style={{minHeight: row.height }}  
                                >
                                    {getComponent({componentName: col.component, options:{title: 'Window ' + count},  layoutInfo:{ row, col, rowIndex, colIndex}})}
                                </Grid>)

                            })}

                       </Grid>
                    )}
                )}
                
            </Grid>
            <Menu
                keepMounted
                open={state.mouseY !== null}
                onClose={closeContextMenu}
                anchorReference="anchorPosition"
                anchorPosition={
                state.mouseY !== null && state.mouseX !== null
                    ? { top: state.mouseY, left: state.mouseX }
                    : undefined
                }
            >
                <MenuItem onClick={addRow}>Add Row</MenuItem>
                <MenuItem onClick={addColumn}>Add Column</MenuItem>
                <MenuItem onClick={deleteRow}>Delete Row</MenuItem>
                <MenuItem onClick={deleteColumn}>Delete Column</MenuItem>
            </Menu>
        </div>

    );
}