import React, {forwardRef, useImperativeHandle} from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const SimpleMenu = forwardRef(({items, layoutInfo}, ref) => {

  const [anchorEl, setAnchorEl] = React.useState(null);

  const open = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const close = () => {
    setAnchorEl(null);
  };

  // expose methods to parent
  useImperativeHandle(ref, () => ({
    open
  }));

  return (
    <div>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={close}
      >
        {items && items.map((item, i) => {
            return (<MenuItem key={`menu-item-${i}`} onClick={() => { item.action({...layoutInfo}); close();}}>{item.label}</MenuItem>);
        })}
      </Menu>
    </div>
  );
});

export default SimpleMenu;